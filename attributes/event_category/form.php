<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
?>
<style type="text/css">
	#color_preview {
		float: left;
	}
    .color_preview {
        width: 32px;
        height: 12px;
        display: block;
        border: 1px solid #c1c1c1;
        margin: 0 22px;
    }
    #add_category{
        float: right;
        margin-right: 18px;
    }
    #event-category-select{
	    margin-left: 72px;
	    max-width: 220px;
    }
</style>
<span id="color_preview">
<?php   if ($categories) { ?>
    <?php   foreach ($categories as $cat) { ?>

        <?php   
        if (in_array($cat['value'],$selected_category)) {
	        ?>
	        <span class="color_preview" style="background-color: #<?php echo $cat['color']?>;"></span>
	        <?php 
		}
		?>
    <?php   } ?>
<?php   } ?>
</span>
<a href="<?php  echo URL::to('/proevents/tools/color_category')?>?akID=<?php  echo $akID?>" id="add_category" class="btn btn-info"><i class="fa fa-plus-circle"></i></a>
<select name="akID[<?php  echo  $akID ?>][value][]" class="form-control" id="event-category-select" multiple="multiple">
    <?php   if ($categories) { ?>
        <?php   foreach ($categories as $cat) { ?>

            <option value="<?php  echo  $cat['value'] ?>" <?php   if (in_array($cat['value'],$selected_category)) {
                echo 'selected';
            } ?>><?php  echo  $cat['value'] ?></option>

        <?php   } ?>
    <?php   } ?>
</select>
<div id="add_value">

</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#add_category').click(function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
               url: url,
               success: function(response){
                   $('#add_value').html(response);
               },
            });
            return false;
        });
        $('#event-category-select option').click(function () {
            var colors = {
            <?php   if($categories){ ?>
	            <?php   foreach($categories as $cat){ ?>
	                '<?php  echo $cat['value']?>': '<?php  echo $cat['color']?>',
	            <?php   } ?>
            <?php   } ?>
            };
            $('#color_preview').html('');
            $.each( $('#event-category-select option:selected'), function( value ) {
	            var $this = $(this);
	            console.log($this.text());
				$('#color_preview').append('<span class="color_preview" style="background-color: #' + colors[$this.text()] + ';"></span>');
			});
        });
       
    });

    var colors = {
    <?php   if($categories){ ?>
	    <?php   foreach($categories as $cat){ ?>
	        '<?php  echo $cat['value']?>': '<?php  echo $cat['color']?>',
	    <?php   } ?>
    <?php   } ?>
    };
</script>
