<?php
defined('C5_EXECUTE') or die(_("Access Denied."));

extract($settings);
$c = Page::getCurrentPage();
$imgHelper = Loader::helper('image');
$eventList = new \Concrete\Package\Proevents\Src\Models\Event();

///////////////////////////////////////////////////////
//This can be removed.  This is really here to explain
//to newer users of C5 that they can easily change
//how PE is displayed. One less support ticket!
///////////////////////////////////////////////////////

if ($c->isEditMode()) {
    echo '<i style="color: orange;max-width: 400px;display: block;">' . t(
            'Don\'t forget, you can quickly change this to Calendar view by clicking on this area, and selecting "Custom Template".<br/><br/>'
        ) . '</i>';
}

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
?>
    <h1 class="hide"><?php   echo $rssTitle ?></h1>

    <div class="ccm-page-list">
        <?php
        if (count($eArray) > 0) {

        foreach ($eArray as $cobj) {
//			var_dump($cobj->getCollectionID());return;
	        $event = $eventList->getByID($cobj->getCollectionID());
//	        var_dump($event);return;
            extract($eventify->getEventListVars($event));
//	        var_dump($eventify->getEventListVars($event));return;

        //###################################################################################//
        //here we lay out they way the page looks, html with all our vars fed to it	     //
        //this is the content that displays.  it is recommended not to edit anything beyond  //
        //the content parse.  Feel free to structure and re-arrange any element and adjust   //
        //CSS as desired.							 	     //
        //###################################################################################//
        ?>
	    <div class="pro_event_page_list">
		    <div class="smallcal">
			    <div class="calwrap">
				    <div class="img hide">
					    <div class="month" <?php   if ($color) {
						    echo 'style="background-color: #' . $color . '!important;"';
					    } ?>>
						    <?php
						    $date_month = date('M', strtotime($date));
						    echo $months[$date_month];
						    ?>
					    </div>
					    <div class="day">
						    <?php echo date('d', strtotime($date)); ?>
					    </div>
				    </div>
			    </div>
			    <div class="infowrap">
				    <div class="titlehead">
					    <div class="title">
						    <?php
						    echo '<h3>' . $title . '<h3>';
						    if ($status) {
							    echo '<span class="' . $status . '"></span>';
						    }
						    ?>
						    <div class="category_colors hide">
							    <?php
							    if (is_array($category)) {
								    foreach ($category as $cat) {
									    $akct = CollectionAttributeKey::getByHandle('event_category');
									    $tcvalue = $event->getAttributeValueObject($akct);
									    $color = $akct->getAttributeType()->getController()->getColorValue($cat);
									    if ($color) {
										    echo '<div class="category_colored" style="background-color: #' . $color . ';"></div>';
									    }
								    }
							    }
							    ?>
						    </div>
					    </div>
					    <div class="local hide">
						    <?php echo $location; ?>
					    </div>
					    <div class="time">
						    <?php
						    if (is_array($next_dates_array)) {
							    foreach ($next_dates_array as $next_date) {
								    echo date(t('M dS '), strtotime($next_date->date));
								    if ($recur == 'daily' && $grouped) {
									    echo t(' - ') . date(t('M dS '), strtotime($thru));
								    }
								    if ($allday != 1) {
									    echo date(t('g:i a'), strtotime($next_date->start)) . ' - ' . date(
											    t('g:i a'),
											    strtotime($next_date->end)
										    ) . '<br/>';
								    } else {
									    // echo ' - ' . t('All Day') . '<br/>';
								    }
							    }
						    }
						    ?>
					    </div>
				    </div>

				    <div class="description">
					    <div class="center thumb">
						    <?php
						    if ($imageF) {
                                $image = $imgHelper->getThumbnail($imageF, 800, 800)->src;
							    echo '<img class="img-responsive" src="' . $image . '"/>';
						    }
						    ?>
					    </div>
					    <?php
					    if ($truncateChars) {
						    print  substr($content, 0, $truncateChars) . '.....';
					    } else {
						    print  $content;
					    }
					    ?>

					    <?php
					    $fc = new GlobalArea('Footer Social');
					    $fc->setAreaGridMaximumColumns(12);
					    $fc->display($c);
					    ?>

				    </div>
				    <!--						<div class="sidebar-form">-->
				    <!--		                    <div class="date-social ccm-block-social-links service-specific">-->
				    <!--			                    --><?php
				    //			                    if ($tweets) {
				    //
				    ?>
				    <!--				                    <span class='st_twitter_hcount share_btn'></span>-->
				    <!--			                    --><?php
				    //			                    }
				    //			                    if ($fb_like) {
				    //
				    ?>
				    <!--				                    <span class='st_facebook_hcount share_btn'></span>-->
				    <!--			                    --><?php
				    //			                    }
				    //			                    if ($google) {
				    //
				    ?>
				    <!--				                    <span class='st_googleplus_hcount share_btn'></span>-->
				    <!--			                    --><?php
				    //			                    }
				    //
				    ?>
				    <!--			                    --><?php
				    //			                    if ($contact_email != '') {
				    //
				    ?>
				    <!--				                    <a href="mailto:-->
				    <?php //  echo $contact_email; ?><!--" class="icon-email"></a>-->
				    <!--				                    <a href="mailto:-->
				    <?php //  echo $contact_email; ?><!--"> contact us</a>-->
				    <!--			                    --><?php //  }?>
				    <!---->
				    <!--			                    <script type="text/javascript">var switchTo5x = true;</script>-->
				    <!--			                    <script type="text/javascript" src="//w.sharethis.com/button/buttons.js"></script>-->
				    <!--			                    <script type="text/javascript">stLight.options({publisher: "--><?php //    echo $sharethis_key;?>
				    <!--	                    "});</script>-->
				    <!--		                    </div>-->
				    <!--						</div>-->

				    <div class="ccm-block-page-list-page-entry-read-more">
					    <?php
					    echo '<a class="ccm-block-page-list-read-more" href="' . $url . '?eID=' . $eID . '">READ MORE</a>';
					    ?>
				    </div>

				    <div class="eventfoot hide">
					    <?php
					    if ($contact_email != '') {
						    ?>
						    <a href="mailto:<?php echo $contact_email; ?>"><?php   echo t(
								    'contact: '
							    ); ?><?php   echo $contact_name ?></a>
					    <?php
					    }
					    if ($contact_email != '' && $address != '') {
						    ?> || <?php
					    }
					    if ($address != '') {
						    ?><a
						    href="http://maps.google.com/maps?f=q&amp;hl=en&amp;&saddr=<?php echo $address; ?>"
						    target="_blank"> <?php echo t('get directions') ?></a> <?php } ?>
				    </div>

			    </div>
		      </div>
		    </div>
		    <?php
		    //#####################################################################################//
		    //this is the end of the recommended content area.  please do not edit below this line //
		    //#####################################################################################//

		    }
		    //is rss feed option is sellected, show it
		    if ($showfeed == 1) {
			    ?>
			    <div class="iCal hide">
				    <p><img src="<?php echo $rss_img_url; ?>" width="25" alt="iCal feed"/>&nbsp;&nbsp;
					    <a href="<?php echo URL::to('/proevents/routes/rss') ?>?bID=<?php echo $bID; ?>&ordering=<?php echo $ordering; ?>"
					       id="getFeed">
						    <?php echo t('get RSS feed'); ?></a></p>
				    <link href="<?php echo URL::to('/proevents/routes/rss') ?>?bID=<?php echo $bID; ?>" rel="alternate"
				          type="application/rss+xml" title="<?php echo t('RSS'); ?>"/>
			    </div>
		    <?php
		    }
        } else {
            echo '<p>' . $nonelistmsg . '</p>';
        }
        ?>
    </div>
    <br style="clear: both;"/>
	<div class="pagination hide">
		<?php
		if ($showPagination){
			echo $pagination;
		}
		?>
	</div>

	<script type="text/javascript">
		$(function(){
			$('.share_btn .st-twitter-counter').removeAttr('style');
			$('.share_btn .st-twitter-counter').addClass('icon-twitter');
			console.info('done');
		});

		$(window).load(function(){
			var prev_url = $('.pagination li.prev a').attr('href');
			var next_url = $('.pagination li.next a').attr('href');
			if(prev_url){
				$('.popular_posts .next a').attr('href',prev_url);
			}else{
				$('.popular_posts .next a').hide();
			}

			if(next_url){
				$('.popular_posts .prev a').attr('href',next_url);
			}else{
				$('.popular_posts .prev a').hide();
			}
		})
	</script>
