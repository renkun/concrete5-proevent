<?php  
namespace Concrete\Package\Proevents\Src\Models;

use Concrete\Core\Search\ItemList\ItemList as AbstractItemList;
use Pagerfanta\Adapter\AdapterInterface;
use Pagerfanta\Pagerfanta;
use Concrete\Core\Search\Pagination\Pagination as Pagination;
use Core;
use Page;

class EventPagination extends Pagination
{


    /**
     * This is a convenience method that does the following: 1. it grabs the pagination/view service (which by default
     * is bootstrap 3) 2. it sets up URLs to start with the pass of the current page, and 3. it uses the default
     * item list query string parameter for paging. If you need more custom functionality you should consider
     * using the Pagerfanta\View\ViewInterface objects directly.
     * @param array
     * @return string
     */
    public function renderDefaultView($c, $arguments = array())
    {
	    $arguments['c'] = $c;
        return $this->renderView('application', $arguments);
    }
    
    public function renderView($driver = 'application', $arguments = array())
    {
        $manager = Core::make('manager/view/pagination');
        $driver = $manager->driver($driver);
        $v = Core::make('\Concrete\Core\Search\Pagination\View\ViewRenderer', array($this, $driver));
        
        $request = \Request::getInstance();
        if($arguments['c'] && !is_object($arguments['c'])){
            $url = $c;
            $ajax = "<script>$('.pagination a').click(function(e){e.preventDefault();getEventResults($(this).attr('href'));return false;})</script>";
        }else{
            if(!$c){$c = Page::getCurrentPage();}
            if(!$c){$c = Page::getByID($request->get('ccID'));}
            $url = $c->getCollectionLink();
        }
        $list = $this->list;
        $html = $v->render(
	        $arguments
        );

        return $html.$ajax;
    }

    public function getCurrentEventResults()
    {
        $this->list->debugStart();

        $results = Pagerfanta::getCurrentPageResults();

        $this->list->debugStop();

        $ereturn = array();

        foreach ($results as $result) {
            $r = $this->list->getEventResult($result);

            if ($r != null) {
                $ereturn[] = $r;
            }
        }

        return $ereturn;
    }

} 